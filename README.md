# ndaal_auditd_public_results



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ndaal_open_source/ndaal_auditd_public_results.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ndaal_open_source/ndaal_auditd_public_results/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


## Scorecard

Aggregate score: 9.2 / 10

Check scores:


|  SCORE  |        NAME         |            REASON             |            DETAILS             |                                             DOCUMENTATION/REMEDIATION                                              |
|---------|---------------------|-------------------------------|--------------------------------|--------------------------------------------------------------------------------------------------------------------|
| 10 / 10 | Binary-Artifacts | no binaries found in the repo |  | https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#binary-artifacts |
| 10 / 10 | Code-Review | all changesets reviewed |  | https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#code-review |
| |   | 1 | 0 |   | / |   | 1 | 0 |   | | |   | C | o | d | e | - | R | e | v | i | e | w |   | | |   | a | l | l |   | c | h | a | n | g | e | s | e | t | s |   | r | e | v | i | e | w | e | d |   | | |   |   | | |   | h | t | t | p | s | : | / | / | g | i | t | h | u | b | . | c | o | m | / | o | s | s | f | / | s | c | o | r | e | c | a | r | d | / | b | l | o | b | / | 4 | 9 | c | 0 | e | e | d | 3 | a | 4 | 2 | 3 | f | 0 | 0 | c | 8 | 7 | 2 | b | 5 | c | 3 | c | 9 | f | 1 | b | b | c | a | 9 | e | 8 | a | a | e | 7 | 9 | 9 | / | d | o | c | s | / | c | h | e | c | k | s | . | m | d | # | c | o | d | e | - | r | e | v | i | e | w |   | |
| |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | / |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | C |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | R |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | a |   | | |   | l |   | | |   | l |   | | |   |   |   | | |   | c |   | | |   | h |   | | |   | a |   | | |   | n |   | | |   | g |   | | |   | e |   | | |   | s |   | | |   | e |   | | |   | t |   | | |   | s |   | | |   |   |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | h |   | | |   | t |   | | |   | t |   | | |   | p |   | | |   | s |   | | |   | : |   | | |   | / |   | | |   | / |   | | |   | g |   | | |   | i |   | | |   | t |   | | |   | h |   | | |   | u |   | | |   | b |   | | |   | . |   | | |   | c |   | | |   | o |   | | |   | m |   | | |   | / |   | | |   | o |   | | |   | s |   | | |   | s |   | | |   | f |   | | |   | / |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | a |   | | |   | r |   | | |   | d |   | | |   | / |   | | |   | b |   | | |   | l |   | | |   | o |   | | |   | b |   | | |   | / |   | | |   | 4 |   | | |   | 9 |   | | |   | c |   | | |   | 0 |   | | |   | e |   | | |   | e |   | | |   | d |   | | |   | 3 |   | | |   | a |   | | |   | 4 |   | | |   | 2 |   | | |   | 3 |   | | |   | f |   | | |   | 0 |   | | |   | 0 |   | | |   | c |   | | |   | 8 |   | | |   | 7 |   | | |   | 2 |   | | |   | b |   | | |   | 5 |   | | |   | c |   | | |   | 3 |   | | |   | c |   | | |   | 9 |   | | |   | f |   | | |   | 1 |   | | |   | b |   | | |   | b |   | | |   | c |   | | |   | a |   | | |   | 9 |   | | |   | e |   | | |   | 8 |   | | |   | a |   | | |   | a |   | | |   | e |   | | |   | 7 |   | | |   | 9 |   | | |   | 9 |   | | |   | / |   | | |   | d |   | | |   | o |   | | |   | c |   | | |   | s |   | | |   | / |   | | |   | c |   | | |   | h |   | | |   | e |   | | |   | c |   | | |   | k |   | | |   | s |   | | |   | . |   | | |   | m |   | | |   | d |   | | |   | # |   | | |   | c |   | | |   | o |   | | |   | d |   | | |   | e |   | | |   | - |   | | |   | r |   | | |   | e |   | | |   | v |   | | |   | i |   | | |   | e |   | | |   | w |   | | |   |   |   | | |   | |
| 0 / 10 | License | license file not detected |  | https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#license |
| |   | 0 |   | / |   | 1 | 0 |   | | |   | L | i | c | e | n | s | e |   | | |   | l | i | c | e | n | s | e |   | f | i | l | e |   | n | o | t |   | d | e | t | e | c | t | e | d |   | | |   |   | | |   | h | t | t | p | s | : | / | / | g | i | t | h | u | b | . | c | o | m | / | o | s | s | f | / | s | c | o | r | e | c | a | r | d | / | b | l | o | b | / | 4 | 9 | c | 0 | e | e | d | 3 | a | 4 | 2 | 3 | f | 0 | 0 | c | 8 | 7 | 2 | b | 5 | c | 3 | c | 9 | f | 1 | b | b | c | a | 9 | e | 8 | a | a | e | 7 | 9 | 9 | / | d | o | c | s | / | c | h | e | c | k | s | . | m | d | # | l | i | c | e | n | s | e |   | |
| |   | | |   |   |   | | |   | 0 |   | | |   |   |   | | |   | / |   | | |   |   |   | | |   | 1 |   | | |   | 0 |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | L |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | f |   | | |   | i |   | | |   | l |   | | |   | e |   | | |   |   |   | | |   | n |   | | |   | o |   | | |   | t |   | | |   |   |   | | |   | d |   | | |   | e |   | | |   | t |   | | |   | e |   | | |   | c |   | | |   | t |   | | |   | e |   | | |   | d |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   |   |   | | |   | | |   | | |   |   |   | | |   | h |   | | |   | t |   | | |   | t |   | | |   | p |   | | |   | s |   | | |   | : |   | | |   | / |   | | |   | / |   | | |   | g |   | | |   | i |   | | |   | t |   | | |   | h |   | | |   | u |   | | |   | b |   | | |   | . |   | | |   | c |   | | |   | o |   | | |   | m |   | | |   | / |   | | |   | o |   | | |   | s |   | | |   | s |   | | |   | f |   | | |   | / |   | | |   | s |   | | |   | c |   | | |   | o |   | | |   | r |   | | |   | e |   | | |   | c |   | | |   | a |   | | |   | r |   | | |   | d |   | | |   | / |   | | |   | b |   | | |   | l |   | | |   | o |   | | |   | b |   | | |   | / |   | | |   | 4 |   | | |   | 9 |   | | |   | c |   | | |   | 0 |   | | |   | e |   | | |   | e |   | | |   | d |   | | |   | 3 |   | | |   | a |   | | |   | 4 |   | | |   | 2 |   | | |   | 3 |   | | |   | f |   | | |   | 0 |   | | |   | 0 |   | | |   | c |   | | |   | 8 |   | | |   | 7 |   | | |   | 2 |   | | |   | b |   | | |   | 5 |   | | |   | c |   | | |   | 3 |   | | |   | c |   | | |   | 9 |   | | |   | f |   | | |   | 1 |   | | |   | b |   | | |   | b |   | | |   | c |   | | |   | a |   | | |   | 9 |   | | |   | e |   | | |   | 8 |   | | |   | a |   | | |   | a |   | | |   | e |   | | |   | 7 |   | | |   | 9 |   | | |   | 9 |   | | |   | / |   | | |   | d |   | | |   | o |   | | |   | c |   | | |   | s |   | | |   | / |   | | |   | c |   | | |   | h |   | | |   | e |   | | |   | c |   | | |   | k |   | | |   | s |   | | |   | . |   | | |   | m |   | | |   | d |   | | |   | # |   | | |   | l |   | | |   | i |   | | |   | c |   | | |   | e |   | | |   | n |   | | |   | s |   | | |   | e |   | | |   |   |   | | |   | |
| 10 / 10 | Security-Policy | security policy file detected | Info: security policy file detected: SECURITY.md:1 Info: Found linked content: SECURITY.md:1 Info: Found disclosure, vulnerability, and/or timelines in security policy: SECURITY.md:1 Info: Found text in security policy: SECURITY.md:1 | https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#security-policy |
| 10 / 10 | Vulnerabilities | no vulnerabilities detected |  | https://github.com/ossf/scorecard/blob/49c0eed3a423f00c872b5c3c9f1bbca9e8aae799/docs/checks.md#vulnerabilities |

## Folder Structure
The Folder Structure consist at least of the following components in our project:

### Basic Folder Structure
The Basic Folder Structure consist at least of the following folders:

#### src Folder
The source code folder! However, in languages that use headers (or if you have a framework for your application) don’t put those files in here.

#### test Folder
Unit tests, integration tests… go here.

#### .config Folder
It should local configuration related to setup on local machine.

#### .build Folder
This folder should contain all scripts related to build process (PowerShell, Docker compose…).

#### dep Folder
This is the directory where all your dependencies should be stored.

#### documentation or doc Folder
The documentation folder

#### repo_check_results Folder
The repo check results folder

#### res Folder
For all static resources in your project. For example, images.

#### samples Folder
Providing “Hello World” & Co code that supports the documentation.

#### tools Folder
Convenience directory for your use. Should contain scripts to automate tasks in the project, for example, build scripts, rename scripts. Usually contains .sh, .cmd files for example.

### Repo folder structure
Git Special Files go here

#### .gitignore File
List of blobs for git to ignore. Affects commands like git add and git clean. You may use gitignore.io to generate a clean and useful gitignore file.
see also <https://gitlab.com/ndaal_open_source/ndaal_public_git_ignore>

#### .gitattributes
Let’s you define git attributes on files (e.g., to change how files look in a diff).

#### .mailmap
Lets you tell git that duplicate names or emails in the history are actually the same person.

#### .gitmodules
Let’s you define submodules (subdirectories of your git repository which are checkouts of other git repositories).

#### .semgrepignore File
The .semgrepignore file describes two types of ignore operations for Semgrep:

- Ignoring as exclusion. Exclude or skip specific files and folders from
  the scope of Semgrep scans in your repository or working directory.
  Ignoring in this context means that Semgrep does not generate findings
  for the ignored files and folders.
- Ignoring as triage action. Ignore specific parts of code that would have
  generated a finding. Ignoring in this context means that Semgrep generates
  a finding record and automatically triages it as Ignored, a triage state.

##### Files/directories
- By default, Semgrep follows the default `.semgrepignore` file.
- If present, Semgrep will look at the repository’s `.gitignore` file.
- In case of a conflict between the two files, the `.semgrepignore` file takes precedence. This means that
  if the `.gitignore` file includes a file and the `.semgrepignore` file excludes it,
  Semgrep will not analyze the file.

##### Example

At least the following define ignored files and folders rules are in place
and active:

```
## Common large paths
node_modules/
build/
dist/
vendor/
.env/
.venv/
.tox/
*.min.js
.npm/
.yarn/

## Common test paths
test/
tests/
*_test.go

## Semgrep rules folder
.semgrep

## Semgrep-action log folder
.semgrep_logs/

## paths 

## files
.gitleaks.toml
.gitleaksignore
.gitignore 
```

#### .ansible-lint File
Ansible Lint is a command-line tool for linting Ansible playbooks, roles and collections aimed toward any Ansible users. 
Its main goal is to promote proven practices, patterns and behaviors while avoiding common pitfalls 
that can easily lead to bugs or make code harder to maintain.

At least our `.ansible-lint` file has the following content:

```
---
##                                                                           #
## Ansible managed
#### .shellcheckrc File
ShellCheck is a static analysis and linting tool for sh/bash scripts. It's mainly focused on handling typical beginner and intermediate level syntax errors and pitfalls where the shell just gives a cryptic error message or strange behavior, but it also reports on a few more advanced issues where corner cases can cause delayed failures.

Unless --norc is used, ShellCheck will look for a file .shellcheckrc or shellcheckrc in the script's directory and each parent directory. If found, it will read key=value pairs from it and treat them as file-wide directives.

Here is an example of `.shellcheckrc`:

```
## Look for 'source'd files relative to the checked script,
## and also look for absolute paths in /mnt/chroot
source-path=SCRIPTDIR
source-path=/mnt/chroot

## Since 0.9.0, values can be quoted with '' or "" to allow spaces
source-path="My Documents/scripts"

## Allow opening any 'source'd file, even if not specified as input
external-sources=true

## Turn on warnings for unquoted variables with safe values
enable=quote-safe-variables

## Turn on warnings for unassigned uppercase variables
enable=check-unassigned-uppercase

## Allow [ ! -z foo ] instead of suggesting -n
disable=SC2236
```

- <https://github.com/koalaman/shellcheck>

#### pyproject.toml File
pyproject.toml file is a configuration file used by packaging tools, as well as other tools such as linters, type checkers, etc. There are three possible TOML tables in this file.

- The [build-system] table is strongly recommended. It allows you to declare which build backend you use and which other dependencies are needed to build your project.

- The [project] table is the format that most build backends use to specify your project’s basic metadata, such as the dependencies, your name, etc.

- The [tool] table has tool-specific subtables, e.g., ```tool.hatch```, ```tool.black```, ```tool.mypy```. We only touch upon this table here because its contents are defined by each tool. Consult the particular tool’s documentation to know what it can contain.

- <https://packaging.python.org/en/latest/guides/writing-pyproject-toml/>

### Git Special Files & Folders

#### README File
README or README.txt or README.md or README.rst etc. is a file that answer the What, Why and How of the project. Git like GitLab or GitHub will recognize and automatically surface the README to repository visitors. Here is an awesome list for more professional readme files.

#### LICENSE File
LICENSE or LICENSE.txt or LICENSE.md etc. is a file that explains the legal licensing, such as any rights, any restrictions, any regulations, etc.

#### CHANGELOG File
CHANGELOG or CHANGELOG.txt or CHANGELOG.md or CHANGELOG.rst etc. is a file that describes what's happening in the repo. Version number increases, software updates, bug fixes… are examples of the file’s content.

#### CONTRIBUTORS File
CONTRIBUTORS or CONTRIBUTORS.txt or CONTRIBUTORS.md etc. is a file that lists people who have contributed to the repo.

#### AUTHORS File
AUTHORS or AUTHORS.txt or AUTHORS.md etc. is a file that lists people who are significant authors of the project, such as the people who are legally related to the work.

#### SUPPORT File
SUPPORT or SUPPORT.txt or SUPPORT.md etc. is a file that explains how a reader can get help with the repository. GitHub links this file on the page "New Issue".

#### SECURITY File
SECURITY (represented by security.txt and available here: <https://ndaal.eu/.well-known/security.txt>) describes our project's security policies, including a list of versions that are currently being maintained with security updates. It also gives instructions on how your users can submit a report of a vulnerability. Fore more details, check the following link.

#### Vulnerability Disclosure Policy File
Vulnerability Disclosure Policy (represented by Vulnerability_Disclosure_Policy.md and available here: <https://gitlab.com/vPierre/ndaal_public_vulnerability_disclosure_policy>) describes our project's Vulnerability Disclosure Policy.

#### CODE_OF_CONDUCT File
CODE_OF_CONDUCT is a file that explains how to engage in a community and how to address any problems among members of your project's community.

Our CODE_OF_CONDUCT based on Contributor Covenant 2.1.

#### CONTRIBUTING File
CONTRIBUTING is a file that explains how people should contribute, and that can help verify people are submitting well-formed pull requests and opening useful issues. GitHub links this file on page "New Issue" and the page "New Pull Request". This helps people understand how to contribute.

#### ACKNOWLEDGMENTS File
ACKNOWLEDGMENTS or ACKNOWLEDGMENTS.txt or ACKNOWLEDGMENTS.md etc. is a file that describes related work, such as other projects that are dependencies, or libraries, or modules, or have their own copyrights or licenses that you want to include in your project.

#### CODEOWNERS File
CODEOWNERS is a file that defines individuals or teams that are responsible for code in a repository. Code owners are automatically requested for review when someone opens a pull request that modifies code that they own. When someone with admin or owner permissions has enabled required reviews, they also can optionally require approval from a code owner before the author can merge a pull request in the repository.

#### FUNDING File
funding.yml is a file to raise funding for or support your project.

#### ISSUE_TEMPLATE File
When you add an issue template to your repository, project contributors will automatically see the template’s contents in the issue body. Templates customize and standardize the information you’d like included when contributors open issues. To add multiple issue templates to a repository create an ISSUE_TEMPLATE/ directory in your project root. Within that ISSUE_TEMPLATE/ directory you can create as many issue templates as you need, for example ISSUE_TEMPLATE/bugs.md. This list contains multiple templates for issues and pull requests.

#### PULL_REQUEST_TEMPLATE File
When you add a PULL_REQUEST_TEMPLATE file to your repository, project contributors will automatically see the template's contents in the pull request body. Templates customize and standardize the information you'd like included when contributors create pull requests. You can create a PULL_REQUEST_TEMPLATE/ subdirectory in any of the supported folders to contain multiple pull request templates.

### Other File(s)
Special Files like the Software Bill of Materials (SBOM) or crawling and indexing information based on robots.txt go here

#### SBOM File
**sbom.json** is a file for Software Bill of Materials (SBOM). A Software Bill of Materials (SBOM) is a complete, formally structured list of components, libraries, and modules that are required to build (i.e. compile and link) a given piece of software and the supply chain relationships between them.

- <https://www.linuxfoundation.org/blog/blog/what-is-an-sbom>
- <https://www.csoonline.com/article/573185/what-is-an-sbom-software-bill-of-materials-explained.html>
- <https://www.nist.gov/itl/executive-order-14028-improving-nations-cybersecurity/software-security-supply-chains-software-1>

#### humans.txt File
What is humans.txt?
It's an initiative for knowing the people behind a website. It's a TXT file that contains information about the different people who have contributed to building the website.
Where is it located?
In the site root. Just next to the robots.txt file.
Why should I?
You don't have to if you don't want. The only aim of this initiative is to know who the authors of the sites we visit are.
Meta Name or humans.txt?
This is not a fight, you don't have to choose one or the other. Humans.txt is just a way to have more information about the authors of the site.
The internet is for humans...
We are always saying that, but the only file we generate is one full of additional information for the searchbots: robots.txt. Then why not doing one for ourselves?

- <https://humanstxt.org/>

#### robots.txt File
The robots.txt file is to prevent the crawling and indexing of certain parts
of your site by web crawlers and spiders run by sites like Yahoo!
and Google. By telling these "robots" where not to go on your site,
you save bandwidth and server resources.

- <https://www.searchenginejournal.com/robots-txt-security-risks/289719/>

Security wise, robots.txt usage has two rules.

Do not try to implement any security through robots.txt. The robots file is
nothing more than a kind suggestion, and while most search engine crawlers
respect it, malicious crawlers have a good laugh and continue at their
business. If it's linked to, it can be found.

Do not expose interesting information through robots.txt. Specifically,
if you rely on the URL to control access to certain resources (which is a
huge alarm bell by itself), adding it to robots.txt will only make the
problem worse: an attacker who scans robots.txt will now see the secret URL
you were trying to hide, and concentrate efforts on that part of your site
(you don't want it indexed, and it's named
'sekrit-admin-part-do-not-tell-anyone', so it's probably interesting).

#### .gitleaks.toml File
The **.gitleaks.toml** file is used by GitLeaks, which is a tool designed for finding sensitive information, such as credentials and secrets, in Git repositories. It's essentially a configuration file that allows you to customize the rules and settings used by GitLeaks during a scan.

Here are some common elements you might find in a **.gitleaks.toml** file:

Rules: Defines the rules that GitLeaks should check for. Each rule typically corresponds to a specific type of sensitive information, such as API keys, passwords, or other credentials.

Example:

```
[[rules]]
  regex = 'AWS[\\s|_]?(ACCESS|SECRET|SESSION)[\\s|_]?(KEY|TOKEN)[\\s|_]?\='
  description = 'AWS Access Key ID or Secret Access Key'
  tags = ['aws', 'access-key', 'secret-key']
Excludes: Lists patterns or paths that should be excluded from the scan. This is useful if you want to skip certain files or directories.
```

Example:

```
[[exclude]]
  pattern = '^vendor/'
Allowlist: Specifies patterns that should be treated as allowed, even if they match a rule. This is useful when you want to exclude specific false positives.
```

Example:

```
[[allowlist]]
  regex = '^foo[\\s|_]bar'
File Types: Specifies the file types/extensions that should be scanned. You can include or exclude specific file types.
```

Example:

```
[filetypes]
  include = ['*']
  exclude = ['*.md']
```

These are just a few examples, and the contents of the .gitlraќs.toml file can vary based on your specific needs and the rules you want to apply. The file is read by GitLeaks to determine how it should conduct the scan and what it should consider as sensitive information.

```
##---------------------------------------------------------------------------#

[allowlist]
description = "global allowlist"
regexes = [
    '''(?:[a-zA-Z]*)(\.)?(?:[a-zA-Z]+)@ndaal.eu''',
    '''https:\/\/ndaal\.eu\/.*''',
    '''D1DE14AA6D1980BD3FB67BF5C706DFBCAC5EFA64''',
]
paths = ['''(\.)?gitleaks_.*\.toml''', '''security.txt''']
```
- <https://github.com/gitleaks/gitleaks>
- <https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository>

## Authors: André Breuer, Pierre Gronau                                      #
## Created: 2024-02-01                                                       #
## Updated: 2024-02-01              #
## This configuration is a global allowlist that extends the default         #
## GitLeaks ruleset found under:                                             #
##  https://github.com/zricethezav/gitleaks/blob/master/config/gitleaks.toml #
## License: MIT                                                              #
## Copyright (C) 2023, 2024 ndaal GmbH & Co KG                               #
## State that this ruleset extends the default ruleset                       #
## Global allowlist for RegEx and Paths                                      #
#### .gitleaksignore File
You can ignore specific findings by creating a **.gitleaksignore** file at the root of your repo. In release v8.10.0 Gitleaks added a Fingerprint value to the Gitleaks report. Each leak, or finding, has a Fingerprint that uniquely identifies a secret. Add this fingerprint to the .gitleaksignore file to ignore that specific secret.

#### .gitlab-ci.yml File
The .gitlab-ci.yml file defines scripts that should be run during the CI/CD pipeline and their scheduling, additional configuration files and templates, dependencies, caches, commands GitLab should run sequentially or in parallel, and instructions on where the application should be deployed to.

GitLab makes it possible to group scripts into jobs that run as part of a larger pipeline, and run them in a specific order. This can be defined within the `.gitlab-ci.yml` file. When you add the file to your repository, GitLab detects it and an application called GitLab Runner runs the scripts defined in the jobs within it.

Example `.gitlab-ci.yml` file:

```
---
stages:
  - test
  - ndaal-secret-detection

ndaal-secrets:
  stage: ndaal-secret-detection
  image:
    name: "zricethezav/gitleaks:latest"
    entrypoint: [""]
  script:
    # yamllint disable-line rule:line-length
    - wget --secure-protocol=auto --https-only --continue --verbose -N --tries=10 --check-certificate -O gitleaks_ndaal.toml https://gitlab.com/ndaal_open_source/ndaal_secretes_search/-/raw/main/gitleaks_base.toml
    - gitleaks -c ./gitleaks_ndaal.toml detect -v -f json -r ndaal-secret-detection-report.json ./
  artifacts:
    reports:
      secret_detection: ndaal-secret-detection-report.json
    expire_in: 4 weeks
  rules:
    - if: $SECRET_DETECTION_DISABLED == 'true' || $SECRET_DETECTION_DISABLED == '1'
      when: never
    - if: $CI_COMMIT_BRANCH

include:
  - template: Security/Secret-Detection.gitlab-ci.yml

```
- <https://docs.gitlab.com/ee/ci/quick_start/>
- <https://docs.gitlab.com/ee/ci/yaml/>

#### structure.txt File
output of a directory tree of the repo to get a quick overview

#### content_summary.txt File
content_summary.txt file is created by the rust program tokei
in our CI/CD pipeline.
Tokei is a program that displays statistics about your code. Tokei will show
the number of files, total lines within those files and code, comments, and
blanks grouped by language.

This file represents a summary of these information.

#### content_long_list.txt File
content_long_list.txt file is created by the rust program tokei
by our CI/CD pipeline.
Tokei is a program that displays statistics about your code. Tokei will show
the number of files, total lines within those files and code, comments, and
blanks grouped by language.

This file represents a detailed overview of these information.

#### scorecard File
Scorecard assesses open source projects for security risks through a series
of automated checks. It was created by OSS developers to help improve
the health of critical projects that the community depends on.

You can use it to proactively assess and make informed decisions about
accepting security risks within your codebase. You can also use the tool
to evaluate other projects and dependencies, and work with maintainers
to improve codebases you might want to integrate.

- <https://securityscorecards.dev/>

We put the scorecard file `scorecard.md` in /documentation/scorecard/

#### checksum File(s)
Checksum files, often accompanied by their **cryptographic** **collision-resistant**
**hash values**, serve the purpose of ensuring the integrity of artifacts
within a public Git repository. These hash values act as unique fingerprints
for files, generated using algorithms like **SHA-256**, **SHA-512**, or others.
By providing checksum files alongside artifacts, contributors and
visitors can:

##### Verify Data Integrity
Checksums act as a digital signature for files, ensuring that the content
hasn't been altered or corrupted.

##### Prevent Tampering
Detect any unauthorized modifications to files, preventing malicious tampering
or unintentional corruption.

##### Ensure Authenticity
Confirm that the artifacts were indeed produced by the expected source
and have not been replaced with malicious versions.

##### Facilitate Secure Downloads
Users can compare the calculated checksum of downloaded files
with the provided checksums to ensure they received
an unmodified copy.

##### Maintain Data Consistency
Provide a reliable mechanism for maintaining consistency
across distributed repositories and diverse contributors.

In summary, checksum files and their associated hash values play
a crucial role in maintaining the trustworthiness and reliability
of artifacts in a public Git repository, fostering a secure and
collaborative development environment.

In our approach, we construct the names of checksum files based
on the original artifact file name and the hash algorithm used.
For example:

For an artifact file named "alpine-standard-3.19.0-aarch64.iso"
using the **blake2b** hash algorithm, the corresponding checksum file name
would be "alpine-standard-3.19.0-aarch64.iso.blake2b".

Similarly, for **SHA-256**, the checksum file name would be
"alpine-standard-3.19.0-aarch64.iso.sha256".

For **k12**, the checksum file name would be
"alpine-standard-3.19.0-aarch64.iso.k12".

This naming convention helps contributors and visitors quickly identify
the checksum files associated with each artifact, streamlining the process
of verifying the integrity of files in the repository. The use
of different hash algorithms provides flexibility and allows users
to choose the level of cryptographic strength based
on their security and compliance requirements.

### Hints
If you see only a **placeholder.txt** File in a folder you can ignore
this directory. It was automatically created by our CI/CD pipeline.

#### .gitkeep .placeholder placeholder.txt approach File(s)
Use an empty file called **.gitkeep** **.placeholder**, **placeholder.txt** in order
to force the presence of the folder in the versioning system.

Although it may seem not such a big difference:

- You use a file that has the single purpose of keeping the folder.
  You don't put there any info you don't want to put.

  For instance, you should use READMEs as, well, READMEs with useful
  information, not as an excuse to keep the folder.

  Separation of concerns is always a good thing, and you can still add a
  .gitignore to ignore unwanted files.

- Naming it .gitkeep, .placeholder, placeholder.txt makes it very clear
  and straightforward from the filename itself (and also to other developers,
  which is good for a shared project and one of the core purposes of a
  Git repository) that this file is

  - A file unrelated to the code (because of the leading dot and the name)
  - A file clearly related to Git
  - Its purpose (keep) is clearly stated and consistent and
    semantically opposed in its meaning to ignore

## GitOps Folder Structure

### GitOps Principles
GitOps is a set of principles for operating and managing software systems.
These principles are derived from modern software operations, but are also rooted in pre-existing and widely adopted best practices.

The desired state of a GitOps managed system MUST be:

1. ## Declarative

    A system managed by GitOps must have its desired state expressed declaratively.

2. ## Versioned and Immutable

    Desired state is stored in a way that enforces immutability, versioning and retains a complete version history.

3. ## Pulled Automatically

    Software agents automatically the desired state declarations from the source.

4. ## Continuously Reconciled

    Software agents continuously observe actual system state and [attempt to apply the desired state.

### Principles
When formulating GitOps standards, it is very important to establish a set of principles as a baseline that all other standards must follow and adhere too. The set of principles being followed here include:

* **Do** separate code (i.e. java, python, etc) from manifests (i.e. yaml) into different repos.
* **Do** minimize yaml duplication, no copy paste
* **Do** support two axis of configuration: clusters and environments (prod, test, dev, etc)
* **Do** be able to split individual cluster and environment configurations into separate repos as needed to support existing organizational practices (i.e. separate production configuration in a different repo from test and dev being the most common example)
* **Do** prefer a multi-folder and/OR multi-repo structure over multi-branch, i.e do not use branching to hold different sets of files (i.e. dev in one branch, test in another). This does not preclude the use of branches for features, this is stating that there should not be permanent branches for clusters or environments.
* **Do** minimize specific GitOps tool (ArgoCD, ACM, etc) dependencies as much as possible
* **Do** put dependent applications manifests in the same manifests repo when managed by the same team. A microservice or 3 tier app that is composed of multiple deployments and managed by the same team would likely be in the same repo. **Do not** put independent applications or applications managed by different teams in the same repo.

### More On Why Not Environment Branches?
So in GitOps you sometimes see organizations using *permanent* branches to represent different environments. In these cases you have a dev branch for the dev environment, a test branch for the test environment, etc.

This often seems like an ideal way to do things, promoting between environments simply comes a matter of merging from lower environment branches to higher environment branches. However in practice it can be quite challenging for the following reasons:

* There are often many files that are environment specific and should either not be merged between environments or need to be named uniquely to avoid collisions
* Typically the 1:1 branch to environment works best when the manifests are identical across all branches, tools like kustomize do not fit into this pattern
* In a microservices world, a one branch per environment will quickly lead to an explosion of branches which again becomes difficult and cumbersome to maintain
* Difficult to have a unified view of cluster state across all environments since the state is stored in separate branches.

So in short I personally much prefer a single branch style with multiple folders to represent environments and clusters as we will see below.

Obviously this does not preclude using branches for updates, PRs, etc but these branches should be short lived, temporary artifacts to support development and not permanent fixtures.

### Assumptions
* As per the [tools standards](https://github.com/gnunn-gitops/standards/blob/master/tools.md), this folder structure is heavily dependent on [kustomize](https://kustomize.io). No thought is given to Helm or other alternatives at this time.
* A lesser used feature of kustomize is its ability to leverage remote repos, i.e. specify a base or overlay from a separate repo. This can be used to isolate environmental configurations in separate repos without duplicating yaml. You can read more about this feature [here](https://github.com/kubernetes-sigs/kustomize/blob/master/examples/remoteBuild.md).
* While the initial structure was focused on Application use cases, I've found it to work well for cluster configuration use cases as well.
* My focus is on [OpenShift](https://www.openshift.com/), I have not vetted anything in this document for other kubernetes distributions however I expect this would work similarly across any distribution.

### Repository Organization
I am a fan of having one or or more catalog repositories to hold common elements that will be re-used across teams and other repositories. You can see this in action with the [Red Hat Canada Catalog](https://github.com/redhat-canada-gitops/catalog) repository where colleagues and I maintain a common set of components that we re-use across our individual repositories.

This is made possible by a great but under-utilized feature of kustomize that enables it to reference remote repositories as a base or resource and then patch it as needed to meet your specific requirements. The key to making this work successfully is to ensure that when you reference the common repository you do so via a tag or commit ID. Not doing this means any time there is an update to the common repo you will automatically get that change deployed by your GitOps tool. Using a tag or commit ID means you control when newer versions are brought in for application by updating it in your git repo.

<b>Note:</b> As an FYI realize in my repos I'm very bad at following this practice of using a tag/commit ID when referencing remote repos, don't be Gerald and do the right thing :)

While in Red Hat Canada we have one repository that covers everything, in many organizations it will be typical to have a few different common repositories maintained by different teams. For example, the operations team may have a common repository for cluster configuration whereas the application architects may maintain a common repository for application components (Nexus, Sonarqube, frameworks, etc).

For Application repositories. in general you should align your repositories along application and team boundaries. For example, if I have an application that consists of a set of microservices where team A manages one microservice and team B manages a different one then this is best done as two different repositories in my opinion. If a team is maintaining multiple applications then again this is likely different repositories, one for each application.

For cluster configuration repositories, I would lean towards having different repositories for each cluster with a common repo for shared components and configuration. Having said that, if you look at my cluster-config repo referenced below I am only using a single repo however my use case is somewhat different then most organizations.

### Folder Layout
Below is the folder standard I use in my own repos, it is relatively opinionated and I am definitely not saying it is the one true way. However given the paucity of documentation in this area I felt it would be useful to put it out there for others as a reference.

<table>
    <colgroup>
        <col />
        <col nowrap />
        <col />
    </colgroup>
    <tr>
        <th></th>
        <th>Folder</th>
        <th>Comments</th>
    </tr>
    <tr>
        <td align="right" valign="top">0</td>
        <td valign="top">├bootstrap</td>
        <td valign="top">
            <ul>
                <li>The minimal yaml required to bootstrap the entity into a cluster. This entity could be an application, cluster configuration or something else.</li>
                <li><em>Generally</em> this will be an Argo CD App of Apps, an ApplicationSet or something of that nature that will in turn load the rest of the components.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">0</td>
        <td valign="top">├components</td>
        <td valign="top">
            <ul>
                <li>Provides yaml for all required apps, pipelines, and GitOps tools.</li>
                <li><em>Generally</em> I prefer to have no buildconfigs (i.e. for s2i) in app folders, those should be in tekton or jenkins pipelines folder</li>
                <li>Avoid creating namespaces in components, this should be done in <em>environments</em> or <em>clusters</em> folders.
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">1</td>
        <td valign="top">├──apps</td>
        <td valign="top">
            <ul>
                <li>No distinction between apps (i.e. code we write) versus services (databases, messaging systems, etc). It’s an artificial distinction IMHO</li>
                <li>Having said that, if your organization feels strongly about services, have peer folder to apps called services</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────{appname1}</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────base</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────overlays</td>
        <td valign="top">
          <ul>
          <li>Overlays related to variations (i.e. HA versus non-HA) as well as environment oriented overlays. Cluster specific overlays should not be here, instead they will be in the `/clusters` folder.</li>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────{appname2}</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────base</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">1</td>
        <td valign="top">├──tekton</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────pipelines</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────{pipeline1}/base</td>
        <td valign="top">
            <ul>
                <li>Includes pipeline plus all artifacts needed for pipeline not called out in separate folders. Items like PVCs for workspaces, buildconfigs used by the pipeline, etc go here</li>
                <li>Pipelines may but do not need to map 1:1 to apps</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────{pipeline2}/base</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────pipelineruns</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────{pipelinerun1}/base</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────{pipelinerun2}/base</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────tasks</td>
        <td valign="top">
            <ul>
                <li>All custom tasks go here, no distinction between shared tasks and pipeline specific tasks. Today’s pipeline specific task is tomorrow’s shared task</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────base</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">1</td>
        <td valign="top">├──jenkins</td>
        <td valign="top">
            <ul>
                <li>If you use Jenkins, I haven't spent much time on Jenkins layout since I'm mostly using tekton these days</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────pipelines</td>
        <td valign="top">
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">1</td>
        <td valign="top">├──argocd</td>
        <td valign="top">
            <ul>
                <li>An optional folder for argocd applicationsthat may be required over and above what is in the bootstrap folder. Most commonly needed when using the App of App pattern with individually defined applications rather then an applicationset.</li>
                <li>In general, put things here if you find you are duplicating argo cd manifests in the bootstrap folder</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">0</td>
        <td valign="top">├environments</td>
        <td valign="top">
            <ul>
                <li>Optional folder that provides additional environment specific kustomization that is separate and distinct from cluster configuration. While components can have environment specific overlays sometimes you need to aggregate different components for a complete application. I like doing this here versus coming up with something artificial in components.</li>
                <li>I also find this useful to support demos which is not a typical use case. As I tend to deploy the same environments to multiple clusters to support these demos having an environments folder makes sense.</li>
                <li><b>Must</b> Inherit from <i>components</i> only, absolutely not permitted to inherit from <i>clusters</i></li>
                <li>When aggregating multiple components namespaces should be created here not in component overlays.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">1</td>
        <td valign="top">├──overlays</td>
        <td valign="top">
            <ul>
                <li>Note that names of overlays are arbitrary, use what works for you.</li>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────dev</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────test</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────prod</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────cicd</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────tools</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">0</td>
        <td valign="top">├clusters</td>
        <td valign="top">
            <ul>
                <li>Cluster specific overlays go here</li>
                <li>Clusters can inherit from <i>environments</i> and <i>components</i></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">1</td>
        <td valign="top">├──overlays</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────{cluster1}</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────dev</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────test</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────cicd</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────tools</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">2</td>
        <td valign="top">├────{cluster2}</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">3</td>
        <td valign="top">├──────prod</td>
        <td valign="top"></td>
    </tr>
    <tr>
        <td align="right" valign="top">0</td>
        <td valign="top">├tenants/{team}</td>
        <td valign="top">
            <ul>
                <li>Only applicable to cluster configuration situations, tenants represent the different teams/applications/etc in a multi-tenant cluster.</li>
                <li>This is where you define cluster level resources (namespaces, quotas, limitranges, operators to install, etc) that are needed by the teams to do their work but typically require cluster-admin rights to provision</li>
            </ul>
        </td>
    </tr>


</table>

### Promoting Manifest Changes
A key question in any GitOps scenario is how to manage promotion of changes in manifests between different environments and clusters. This process is heavily dependent on the structure and processes of the organization, however it is possible to define some basic characteristics that we are looking for as follows:

* Since every overlay depends on the base manifests, every change in the manifests needs to flow through the environments in hierarchical order, i.e. (dev > test > prod). We do not want a change to a base flowing to all environments simultaneously.
* To prevent changes in manifests flowing directly to environments, the state of environments and clusters needs to be pinned in git (i.e. commit revision or tag).
* Changes to environments/clusters can flow directly to the target environment. i.e. a direct change to the prod overlay can flow directly to prod without a promotion process. However given the structure of our repo these direct changes should be rare (i.e. prod specific secrets, etc) and limited to emergencies.

So as stated above, we need to tie specific environments to specific revisions so that changes in the repo can be promoted in a controlled manner following a proper SDLC process. Both the various GitOps tools (ArgoCD, Flux, ACM, etc) and Kustomize support referencing specific commits in a repo, as a result there are various options we have for managing environment promotions.

Note that using branches is implicit in these options but not discussed directly. As per the Why Not Branches section above, the intent is for short-lived branches to be created for revisions and merged back to trunk. So managing revisions is really about tracking the appropriate revision in trunk.

##### Option 1 - Manage revisions in the GitOps Tool
In this option we deploy each environment as an independent entity in the GitOps tool and tie each environment to a specific revision. So in ArgoCD, we would tie the application object for the Dev environment to one revision, the Test environment to another revision, etc.

When we are ready to promote a change, we simply update the revision in the GitOps tool to reference the appropriate revision or tag in the repo.

This is simple to do however it does require active management of the GitOps tools entities.

##### Option 2 - Manage revisions in kustomize
In this option each environment we deploy uses kustomize to manage the git revision. By default you tie kustomize to the local directory, i.e:

```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

namespace: product-catalog-dev

bases:
- ../../../manifests/app/database/base
- ../../../manifests/app/server/base
- ../../../manifests/app/client/base
```

However kustomize supports remote references so you can also reference the bases remotely with specific git revisions:

```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

namespace: product-catalog-dev

bases:
- https://github.com/gnunn-gitops/product-catalog/manifests/app/database/base?ref=9769ad7
- https://github.com/gnunn-gitops/product-catalog/manifests/app/server/base?ref=9769ad7
- https://github.com/gnunn-gitops/product-catalog/manifests/app/client/base?ref=9769ad7
```

In this way we can promote changes across multiple environments by simply updating the reference accordingly.

This approach provides very explicit control over the bases at the slight cost of having kustomize perform additional clones of the repo.

##### Option 3 - Hybrid (Do Both)
In this option we combine Options #1 and #2 for maximum control.

##### Recommendation
I don't have strong feelings at this point but my personal leaning is towards Hybrid.

### Examples
Here are a couple of repositories where you can see this standard in action:

* [Product Catalog](https://github.com/gnunn-gitops/product-catalog). This is a three tier application (front-end, back-end and database) deployed using GitOps with ArgoCD (or ACM) and kustomize. It deploys three separate environments (dev, test and prod) along wth Tekton pipelines to build the front-end and back-end applications. It also deploys a grafana instance for application monitoring that ties into OpenShift's [user defined monitoring](https://docs.openshift.com/container-platform/4.6/monitoring/enabling-monitoring-for-user-defined-projects.html).
* Cluster Configuration (<https://github.com/gnunn-gitops/cluster-config>). This repo shows how I configure my OpenShift clusters using GitOps with ArgoCD. It configures a number of things including certificates, authentication, default operators, console customizations, storage and more.

I also highly recommend checking out the [Red Hat Canada GitOps](https://github.com/redhat-canada-gitops) organization as well. These repos include a default installation of the excellent Jenkins operator as well as a [catalog](https://www.jenkins.io/projects/jenkins-operator/) of tools and applications deployed with kustomize.

## Ansible Role directory structure

### Option 1: one size fits all
Here is a suggested directory layout from the [https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html?highlight=roles#directory-layout Ansible documentation].

group_vars directory: contains yaml files that define variables for each of the groups we define.

host_vars directory: contains yaml files that define variables for each of the hosts.

library, module_utils, and filter_utils directories: (not currently using any custom filters or modules... but this is where you would put them)

site.yml is the master playbook that defines all the plays.

There is also a sub-playbook for each role - for example, web servers or database servers.

Each role can either use the default, or define its own set of each of the following:
* tasks
* handlers
* templates
* files
* variables
* default values

(Roles expect this info to be contained in a main.yml file.)

The "common" folder defines the default values of all of the above, for all roles.

The "webserver" folder would define the above for all hosts playing a webserver role.

The "fooapp" folder would define the above for all hosts hosting the fooapp.

etc...

<pre>
hosts                     # inventory file

group_vars/
   group1.yml             # here we assign variables to particular groups
   group2.yml
host_vars/
   hostname1.yml          # here we assign variables to particular systems
   hostname2.yml

library/                  # if any custom modules, put them here (optional)
module_utils/             # if any custom module_utils to support modules, put them here (optional)
filter_plugins/           # if any custom filter plugins, put them here (optional)

site.yml                  # master playbook
webservers.yml            # playbook for webservers role
dbservers.yml             # playbook for dbservers role
fooapp.yml                # playbook for foo app

roles/
    common/               # this hierarchy represents defaults for a "role"
        tasks/            #
            main.yml      #  <-- tasks file can include smaller files if warranted
        handlers/         #
            main.yml      #  <-- handlers file
        templates/        #  <-- files for use with the template resource
            ntp.conf.j2   #  <------- templates end in .j2
        files/            #
            bar.txt       #  <-- files for use with the copy resource
            foo.sh        #  <-- script files for use with the script resource
        vars/             #
            main.yml      #  <-- variables associated with this role
        defaults/         #
            main.yml      #  <-- default lower priority variables for this role
        meta/             #
            main.yml      #  <-- role dependencies
        library/          # roles can also include custom modules
        module_utils/     # roles can also include custom module_utils
        lookup_plugins/   # or other types of plugins, like lookup in this case

    webservers/           # same kind of structure as "common" was above, done for the webservers role
    dbservers/            # ""
    fooapp/               # ""
</pre>

For details about each of these files (e.g., how to include what with what, how to name things in groups to work correctly, etc), see [[Ansible/Directory Layout/Details]]

### Option 2: separate environments
An alternative layout that keeps the inventory file with the corresponding group and host variable files.

This is useful when the variable files for each environment are not alike, so it is easier not to try and share things.

<pre>
inventories/
   production/
      hosts               # inventory file for production servers
      group_vars/
         group1.yml       # here we assign variables to particular groups
         group2.yml
      host_vars/
         hostname1.yml    # here we assign variables to particular systems
         hostname2.yml

   staging/
      hosts               # inventory file for staging environment
      group_vars/
         group1.yml       # here we assign variables to particular groups
         group2.yml
      host_vars/
         stagehost1.yml   # here we assign variables to particular systems
         stagehost2.yml

library/
module_utils/
filter_plugins/

site.yml
webservers.yml
dbservers.yml

roles/
    common/
    webtier/
    monitoring/
    fooapp/

</pre>

==Option 3: Ansible Galaxy==

While the main role of ansible galaxy is to download roles shared by the Ansible user community, it can also be used to create a directory structure that can be populated for roles.

Use the <code>ansible-galaxy init</code> command to initialize a directory structure:

<pre>
$ ansible-galaxy init -p <path-to-roles-directory> <name-of-role>
</pre>

For example:

<pre>
$ ansible-galaxy init -p playbooks/roles web
</pre>

If the -p flag is not specified, the role files are created in the current directory.

Running this command creates the following files and directories:

<pre>
playbooks
└── roles
    └── web
        ├── README.md
        ├── defaults
        │   └── main.yml
        ├── files
        ├── handlers
        │   └── main.yml
        ├── meta
        │   └── main.yml
        ├── tasks
        │   └── main.yml
        ├── templates
        ├── tests
        │   ├── inventory
        │   └── test.yml
        └── vars
            └── main.yml
</pre>

- <https://github.com/redhat-cop/automation-good-practices>
- <https://charlesreid1.com/wiki/Ansible/Directory_Layout/Details>

## Terraform

- <https://github.com/hashicorp/terraform>
- <https://github.com/MarcinKasprowicz/ultimate-terraform-folder-structure>
- <https://www.terraform-best-practices.com/code-structure>
- <https://github.com/antonbabenko/terraform-best-practices>
- <https://github.com/enginyoyen/ansible-best-practises>

## Pulumi

- <https://github.com/pulumi/pulumi>
- <https://github.com/pulumi/examples>
- <https://gist.github.com/tksrc/9e6a6bcdb488b3fa1df07645029472b2>
